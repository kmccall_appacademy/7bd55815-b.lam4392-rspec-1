def translate(phrase)
  pig_latin = []
  phrase.split.each do |word|
    pig_word = pig_latinize(word.downcase)
    if word[0] == word[0].upcase
      pig_word = pig_word.capitalize
    end

    if '.,:;!?'.include?(word[-1])
      pig_word += word[-1]
    end

    pig_latin << pig_word
  end
  pig_latin.join(' ')
end

def pig_latinize(word)
  word.delete!('.,:;!?')
  pig_word = ''
  substr = ''
  vowels = 'aeiou'

  return word + 'ay' if vowels.include?(word[0].downcase)

  word.each_char.with_index do |letter, idx|
    if word[idx..idx+1].downcase == 'qu' || word[idx-1..idx].downcase == 'qu'
      substr += letter
    elsif vowels.include?(letter.downcase)
      pig_word = word[idx..-1] + substr + 'ay'
      break
    else
      substr += letter
    end
  end
  pig_word
end
