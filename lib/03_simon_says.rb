def echo(phrase)
  phrase
end

def shout(phrase)
  phrase.upcase
end

def repeat(phrase, num_times = 2)
  repeat = []
  num_times.times do
    repeat << phrase
  end
  repeat.join(' ')
end

def start_of_word(phrase, num_letters)
  phrase[0...num_letters]
end

def first_word(phrase)
  phrase.split.first
end

def titleize(phrase)
  words = phrase.split
  words.each_with_index do |word, idx|
    if word.length >= 5 || word == words.first || word == words.last
      words[idx] = word.capitalize
    end
  end
  words.join(' ')
end
